﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using FlexMigrations;
using Microsoft.Extensions.Logging;
using Npgsql;

namespace PostgreSqlMigrationSample
{
  public class MigrationFacade
  {
    public void Run(ILoggerFactory loggerFactory, string connectionString)
    {
      try
      {
        IDbConnectionFactory connectionFactory = new DbConnectionFactory<NpgsqlConnection>(connectionString);
        MigrationManager migrationManager = new MigrationManager(loggerFactory);
        migrationManager.RunMigrations(connectionFactory, ErrorPolicy.AllOrNothing, false);
      }
      catch (Exception e)
      {
        Console.WriteLine("Failed to migrate DB: " + e.Message);
      }
    } 
  }
}
