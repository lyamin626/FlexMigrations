﻿namespace FlexMigrations
{
  public enum MigrationStatusType
  {
    Undefined,

    /// <summary>
    /// Database version is equivalent to script version
    /// </summary>
    DbVersionEqual,

    /// <summary>
    /// The version of the database is greater than the version of the scripts
    /// </summary>
    DbVersionHigher,

    /// <summary>
    /// The version of the database is less than the version of the scripts
    /// </summary>
    DbVersionLower,
  }
}