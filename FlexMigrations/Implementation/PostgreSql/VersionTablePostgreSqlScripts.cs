﻿using System.Collections.Generic;

namespace FlexMigrations.Implementation.PostgreSql
{
  internal class VersionTablePostgreSqlScripts : VersionTableScriptsBase
  {
    public override List<string> CreateScripts => new List<string>
    {
      $"CREATE TABLE {TableName} (" +
      $" {Version} INTEGER NOT NULL," +
      $" {ScriptName} TEXT NOT NULL, " +
      $" {DateName}	TIMESTAMP NOT NULL DEFAULT current_timestamp, " +
      $"PRIMARY KEY({Version}));",

      $"INSERT INTO {TableName}({Version}, {ScriptName}) VALUES (0, 'initial');"
    };

    public override string CheckExistence =>
      $"SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = '{TableName}')";

    public override string GetDbVersion => $"SELECT MAX({Version}) FROM {TableName}";

    public override string SetDbVersion(int version, string scriptName) =>
      $"INSERT INTO {TableName}({Version}, {ScriptName}) VALUES ('{version}', '{scriptName}')";

    public override bool ProcessCheckExistenceRes(object sqlRes)
    {
      return (bool) sqlRes;
    }

    public override int ProcessGetDbVersionRes(object sqlRes)
    {
      return (int)sqlRes;
    }
  }
}
