﻿using System.Collections.Generic;

namespace FlexMigrations.Implementation.SQLite
{
  internal class VersionTableSqliteScripts : VersionTableScriptsBase
  {
    public override List<string> CreateScripts => new List<string>
    {
      $"CREATE TABLE '{TableName}' (" +
      $" '{Version}' INTEGER NOT NULL," +
      $" '{ScriptName}' TEXT NOT NULL, " +
      $" '{DateName}'	TIMESTAMP NOT NULL DEFAULT current_timestamp, " +
      $"PRIMARY KEY('{Version}'));",

      $"INSERT INTO '{TableName}'('{Version}', '{ScriptName}') VALUES (0, 'initial');"
    };

    public override string CheckExistence =>
      $"SELECT name FROM sqlite_master WHERE type='table' AND name='{TableName}';";

    public override string GetDbVersion => $"SELECT MAX({Version}) FROM '{TableName}'";

    public override string SetDbVersion(int version, string scriptName) =>
      $"INSERT INTO '{TableName}'('{Version}', '{ScriptName}') VALUES ('{version}', '{scriptName}')";

    public override bool ProcessCheckExistenceRes(object sqlRes)
    {
      string tableName = (string)sqlRes;
      return !string.IsNullOrEmpty(tableName);
    }

    public override int ProcessGetDbVersionRes(object sqlRes)
    {
      long res = (long) sqlRes;
      return (int) res;
    }
  }
}
