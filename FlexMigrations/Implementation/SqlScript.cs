﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("FlexMigrations.Test")]
namespace FlexMigrations.Implementation
{
  internal class SqlScript
  {
    public int Index { get; }
    public string FullName { get; }
    public string ShortName { get; }
    public string Content { get; private set; }

    private const string Suffix = ".sql";

    public bool IsValid => Index > 0;

    public SqlScript(string fullName)
    {
      FullName = fullName;

      if (fullName == null || !fullName.EndsWith(Suffix, StringComparison.InvariantCultureIgnoreCase))
      {
        ShortName = fullName;
        Index = -1;
        return;
      }

      int lastDotIdx = CalcLastDotIdx(fullName);
      if (lastDotIdx == -1 || lastDotIdx == fullName.Length - Suffix.Length - 1)
        ShortName = fullName;
      else
        ShortName = fullName.Substring(lastDotIdx + 1);

      Index = ExtractScriptIndex();
    }

    private int CalcLastDotIdx(string str)
    {
      if (string.IsNullOrEmpty(str))
        return -1;

      string woExtension = str.Substring(0, str.Length - Suffix.Length);
      int idx = woExtension.LastIndexOf(".", StringComparison.Ordinal);

      return idx;
    }

    private int ExtractScriptIndex()
    {
      if (string.IsNullOrEmpty(ShortName))
        return -1;

      int firstNonDigitIdx = 0;
      for (int i = 0; i < ShortName.Length; ++i)
      {
        char c = ShortName[i];
        if (!char.IsDigit(c))
        {
          firstNonDigitIdx = i;
          break;
        }
      }

      if (firstNonDigitIdx <= 0)
        return -1;

      string idxStr = ShortName.Substring(0, firstNonDigitIdx);

      int res;
      if (!int.TryParse(idxStr, out res))
        return -1;

      return res;
    }

    public SqlScript ReadContent(Stream stream)
    {
      using (StreamReader resourceStreamReader = new StreamReader(stream, Encoding.Default, true))
        Content = resourceStreamReader.ReadToEnd();

      return this;
    }
  }
}
