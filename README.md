# Description
FlexMigrations is the .Net Standard library for applying migrations to DB.  
Special table (db_version, by default) is used for storing current version of DB.  
When migration process is launched it compares current set of migration scripts with DB version and applies new scripts. At the end, DB version is set to latest applied script.

## Features:
* Simple to use - just couple lines of code are required.
* Supported DBs: SQL Server, SQLite, PostgreSql.
* Migrations are expected as .sql files, embedded into user's assembly.
* Provides possibility to lock db during migrations.
* Provides possibility to customize db version table.  
* Provides possibility to select behaviour in case of error during migration: apply successfull migrations or discard all.
  
## Installation
NuGet package FlexMigrations is available

### Usage  
1.  Prepare your .sql migration scripts, include them into .net project and set Build Action = "Embedded Resource".  
Naming restriction: each script MUST start with number, optionally followed by description. Number can start with zeros. Exactly that number is written to DB as DB version after the script is applied.  
Example: 001_CreateUsers.sql  
2.  Insert the following 2 lines of code (note that it can throw runtime exception):
```
IDbConnectionFactory connectionFactory = new DbConnectionFactory<SQLiteConnection>(connectionStr);
MigrationManager.RunMigrations(connectionFactory, ErrorPolicy.AllOrNothing, false);
```
Note: DbConnectionFactory should be typed with class depending on your DB:
* SQLServer - SqlConnection  
* SQLite - SQLiteConnection  
* NpgsqlConnection - PostgreSql  
Other DBs are not supported yet.

### Parameters
The full definition of RunMigrations is  
```
public static bool RunMigrations(  
  IDbConnectionFactory connectionFactory,  
  ErrorPolicy errorPolicy,  
  bool lockDb,  
  IVersionTableScripts versionTableScripts = null,  
  string preFlexDbVersionQuery = null,  
  Assembly assembly = null,  
  int cmdTimeoutSec = 30)  
```
* connectionFactory - instance of class which can create connections to your database.
* errorPolicy - determines behavior in case of error during migration process. Two options are available:  
  * ErrorPolicy.AllOrNothing - all migrations are executed in one transaction, so in case of error all migrations are rolled back.
  * ErrorPolicy.AsMuchAsPossible - each migration is executed in its own transaction, so in case of error only last (erroneous) migration is rolled back.
* lockDb - specifies whether DB should be protected from other changes during migration process. If set, then SQL Server DB is switched to RESTRICTED MODE, so that only users with administrative/dbowner privilegies can connect to it.  
* versionTableScripts - user-defined scripts for db_version table, described below in detail
* preFlexDbVersionQuery - for one-time migration from legacy migration table to the new one. Should be an SQL which returns integer - current DB version.  
* assembly - assembly which contains embedded migration scripts. By default a calling assembly is used.
* The time in seconds to wait for an each SQL migration command to execute. Default is 30 seconds.

### Custom version table
By default, the table is called db_version and has the following structure:
* int version, primary key  
* nvarchar(50) script_name, not null - contains name of last applied migration script
* datetime date - contains date&time of last applied migration script

In case if this is not suitable, a custom table can be used by supplying custom implementation of IVersionTableScripts to RunMigrations method.  
  
An implementation should define
* CreateScripts - list of scripts which create the user-defined table
* CheckExistence - sql script which should return whether db version table already exists in DB or not
* GetDbVersion - sql script which should return current version of DB
* SetDbVersion - should return sql script which writes provided version to DB
* ProcessCheckExistenceRes(object sqlRes) - converts result of your CheckExistence query to boolean value (true if table already exists)
* ProcessGetDbVersionRes(object sqlRes) - converts result of your GetDbVersion query to int value - current DB version

## TODO  
Add support for other DBs.